import React from "react";

function About() {
    return (
        <div id="about" className="about segments">
            <div className="container">
                <div className="box-content">
                    <div className="row">
                        <div className="col-md-6 col-sm-12 col-xs-12">
                            <div className="content-left">
                                <div className="section-title section-title-left">
                                    <h3>About Me</h3>
                                </div>
                                <div className="content">
                                    <h2>I am a Graphic Designer</h2>
                                    <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ut doloremque ratione perferendis possimus voluptatibus distinctio
                                autem expedita qui unde modi impedit officia illum praesentium amet, vero quos natus veritatis totam!</p>
                                </div>
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-12 col-xs-12">
                            <div className="content-right"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { About };
