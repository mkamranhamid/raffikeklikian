import React from "react";

function Welcome() {
    return (
        <div className="home-intro segments">
            <div className="container">
                <div className="intro-content box-content">
                    <div className="row justify-content-center">
                        <div className="col-md-8 col-sm-12 col-xs-12">
                            <div className="intro-caption">
                                <span>I am Bleak Peaker</span>
                                <h2>Graphic Designer</h2>
                                <button className="button">Contact Me</button>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-12 col-xs-12">
                            <div className="intro-image">
                                <img src="../intro-image.png" alt="" />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Welcome };
