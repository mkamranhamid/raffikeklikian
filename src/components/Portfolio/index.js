import React from "react";
import data from "./data";
import { Project } from "../Project";

function Portfolio() {
    console.log(data)
    let { portfolio: { projects, categories } } = data;
    return (
        <div id="portfolio" className="portfolio segments">
            <div className="container">
                <div className="box-content">
                    <div className="section-title">
                        <h3>My Portfolio</h3>
                    </div>
                    <div className="portfolio-filter-menu">
                        <ul>
                            <li data-filter="all" className="active">
                                <span>See All</span>
                            </li>
                            <li data-filter="1">
                                <span>House Plant</span>
                            </li>
                            <li data-filter="2">
                                <span>Flowers</span>
                            </li>
                            <li data-filter="3">
                                <span>Photography</span>
                            </li>
                        </ul>
                    </div>

                    <div className="row no-gutters filtr-container">

                        {projects.map((project, ind) => <Project key={ind} data={project} index={ind} />)}

                        {/* <div className="col-md-4 col-sm-12 col-xs-12 filtr-item" data-category="3">
                            <div className="content-image">
                                <a href="../portfolio1.jpg" className="portfolio-popup">
                                    <img src="../portfolio1.jpg" alt="" />
                                    <div className="image-overlay"></div>
                                    <div className="portfolio-caption">
                                        <div className="title">
                                            <h4>Classic Minimalist Door</h4>
                                        </div>
                                        <div className="subtitle">
                                            <span>Graphic Design</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-12 col-xs-12 filtr-item" data-category="1">
                            <div className="content-image">
                                <a href="../portfolio2.jpg" className="portfolio-popup">
                                    <img src="../portfolio2.jpg" alt="" />
                                    <div className="image-overlay"></div>
                                    <div className="portfolio-caption">
                                        <div className="title">
                                            <h4>Green Succulent Plants</h4>
                                        </div>
                                        <div className="subtitle">
                                            <span>Nature Plant</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-12 col-xs-12 filtr-item" data-category="2, 1">
                            <div className="content-image">
                                <a href="../portfolio3.jpg" className="portfolio-popup">
                                    <img src="../portfolio3.jpg" alt="" />
                                    <div className="image-overlay"></div>
                                    <div className="portfolio-caption">
                                        <div className="title">
                                            <h4>Yellow Rose Pale Plant</h4>
                                        </div>
                                        <div className="subtitle">
                                            <span>Flower Plant</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-12 col-xs-12 filtr-item" data-category="3">
                            <div className="content-image">
                                <a href="../portfolio4.jpg" className="portfolio-popup">
                                    <img src="../portfolio4.jpg" alt="" />
                                    <div className="image-overlay"></div>
                                    <div className="portfolio-caption">
                                        <div className="title">
                                            <h4>Succulent Plant in Pot</h4>
                                        </div>
                                        <div className="subtitle">
                                            <span>Plant House</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-12 col-xs-12 filtr-item" data-category="1, 2">
                            <div className="content-image">
                                <a href="../portfolio5.jpg" className="portfolio-popup">
                                    <img src="../portfolio5.jpg" alt="" />
                                    <div className="image-overlay"></div>
                                    <div className="portfolio-caption">
                                        <div className="title">
                                            <h4>Photo of Green Cactus</h4>
                                        </div>
                                        <div className="subtitle">
                                            <span>Plant House</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div>
                        <div className="col-md-4 col-sm-12 col-xs-12 filtr-item" data-category="3, 1">
                            <div className="content-image">
                                <a href="../portfolio6.jpg" className="portfolio-popup">
                                    <img src="../portfolio6.jpg" alt="" />
                                    <div className="image-overlay"></div>
                                    <div className="portfolio-caption">
                                        <div className="title">
                                            <h4>Two Feet of Two Ferns</h4>
                                        </div>
                                        <div className="subtitle">
                                            <span>Graphic Design</span>
                                        </div>
                                    </div>
                                </a>
                            </div>
                        </div> */}
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Portfolio };
