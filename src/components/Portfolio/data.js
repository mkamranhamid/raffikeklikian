export default {
    portfolio: {
        ttle: "My Portfolio",
        categories: [
            {
                frequency: 'all',
                title: 'See All'
            },
            {
                frequency: '1',
                title: 'House Plant'
            },
            {
                frequency: '2',
                title: 'Flowers'
            },
            {
                frequency: '3',
                title: 'Photography'
            },
        ],
        projects: [
            {
                title: "Classic Minimalist Door",
                subtitle: "Graphic Design",
                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic",
                category: "3",
                link: "https://www.lipsum.com/",
                img_source: "../portfolio1.jpg",
            },
            {
                title: "Green Succulent Plants",
                subtitle: "Nature Plant",
                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic",
                category: "1",
                link: "https://www.lipsum.com/",
                img_source: "../portfolio2.jpg",
            },
            {
                title: "Yellow Rose Pale Plant",
                subtitle: "Flower Plant",
                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic",
                category: "2, 1",
                link: "https://www.lipsum.com/",
                img_source: "../portfolio3.jpg",
            },
            {
                title: "Succulent Plant in Pot",
                subtitle: "Plant House",
                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic",
                category: "3",
                link: "https://www.lipsum.com/",
                img_source: "../portfolio4.jpg",
            },
            {
                title: "Photo of Green Cactus",
                subtitle: "Plant House",
                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic",
                category: "1, 2",
                link: "https://www.lipsum.com/",
                img_source: "../portfolio5.jpg",
            },
            {
                title: "Two Feet of Two Ferns",
                subtitle: "Graphic Design",
                description: "Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic",
                category: "3, 1",
                link: "https://www.lipsum.com/",
                img_source: "../portfolio5.jpg",
            },
        ]
    }
}