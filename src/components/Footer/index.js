import React from "react";

function Footer() {
    return (
        <div className="footer segments">
            <div className="container">
                <div className="box-content">
                    <p>Copyright © All Right Reserved</p>
                </div>
            </div>
        </div>
    )
}

export { Footer };
