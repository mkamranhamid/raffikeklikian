import React from "react"
import Helmet from "react-helmet"
import { withPrefix } from "gatsby"
import { Header } from "../Header"

function Layout({ children }) {
    return (
        <>
            <Helmet>
                <meta charset="utf-8" />
                <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no" />
                <meta name="description" content="Portfolio, raffl" />
                <meta name="author" content="Portfolio, raffl" />

                <title>Raff Portfolio</title>
                <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,400i,500,500i,700,900&display=swap" rel="stylesheet"></link>
                {/* <script src={withPrefix('../../scripts/jquery.min.js')} type="text/javascript" /> */}
                <script
                    src="https://code.jquery.com/jquery-3.5.1.min.js"
                    integrity="sha256-9/aliU8dGd2tb6OSsuzixeV4y/faTqgFtohetphbbj0="
                    crossOrigin="anonymous"
                ></script>
            </Helmet>
            <div>
                {children}
            </div>

            <Helmet>
                {/* <script src={withPrefix('../../scripts/jquery.min.js')} type="text/javascript" /> */}
                <script src={withPrefix('../../scripts/bootstrap.min.js')} type="text/javascript" />
                {/* <script src={withPrefix('../../scripts/fakeLoader.min.js')} type="text/javascript" /> */}
                <script src={withPrefix('../../scripts/owl.carousel.min.js')} type="text/javascript" />
                <script src={withPrefix('../../scripts/jquery.filterizr.min.js')} type="text/javascript" />
                <script src={withPrefix('../../scripts/imagesloaded.pkgd.min.js')} type="text/javascript" />
                <script src={withPrefix('../../scripts/jquery.magnific-popup.min.js')} type="text/javascript" />
                <script src={withPrefix('../../scripts/contact-form.js')} type="text/javascript" />
                <script src={withPrefix('../../scripts/main.js')} type="text/javascript" />
            </Helmet>
        </>
    )
}

export { Layout };
