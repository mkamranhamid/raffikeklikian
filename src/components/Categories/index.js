import React, { useState } from "react";

function Categories({ data }) {
    const [selectedCategory, setCategory] = useState('all');
    return (
        <ul>
            {
                data.map((category, ind) => (
                    <li
                        key={category.title + ind}
                        data-filter={category.frequency}
                        onClick={() => setCategory(category.frequency)}
                        className={category.frequency == selectedCategory ? 'active' : ''}>
                        <span>{category.title}</span>
                    </li>
                ))
            }
        </ul>
    )
}

export { Categories };
