import React from "react";

function Contact() {
    return (
        <div id="contact" className="contact segments">
            <div className="container">
                <div className="box-content">
                    <div className="row">
                        <div className="col-md-6 col-sm-12 col-xs-12">
                            <div className="content-left">
                                <div className="section-title section-title-left">
                                    <h3>Contact Me</h3>
                                </div>
                                <h2>Realize your dream with us</h2>
                                <ul>
                                    <li>
                                        <a href="#"><i className="fab fa-facebook-f"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fab fa-twitter"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fab fa-dribbble"></i></a>
                                    </li>
                                    <li>
                                        <a href="#"><i className="fab fa-google"></i></a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                        <div className="col-md-6 col-sm-12 col-xs-12">
                            <div className="content-right">
                                <form action="contact-form.php" className="contact-form" id="contact-form" method="post">
                                    <div className="row">
                                        <div className="col">
                                            <div id="first-name-field">
                                                <input type="text" placeholder="First Name" className="form-control" name="form-name" />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div id="last-name-field">
                                                <input type="text" placeholder="Last Name" className="form-control" name="form-name" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div id="email-field">
                                                <input type="email" placeholder="Email Address" className="form-control" name="form-email" />
                                            </div>
                                        </div>
                                        <div className="col">
                                            <div id="subject-field">
                                                <input type="text" placeholder="Subject" className="form-control" name="form-subject" />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col">
                                            <div id="message-field">
                                                <textarea cols="30" rows="5" className="form-control" id="form-message" name="form-message" placeholder="Message"></textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <button className="button" type="submit" id="submit" name="submit">Send Message</button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    )
}

export { Contact };
