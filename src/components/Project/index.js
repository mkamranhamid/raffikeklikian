import React from "react";

function Project({ data, index }) {
    return (
        <>
            <div className="col-md-4 col-sm-12 col-xs-12 filtr-item" data-category={data.category}>
                <div className="content-image">
                    <a data-toggle="modal" data-target={`#exampleModal${index}`}>
                        <img src={data.img_source} alt="" />
                        <div className="image-overlay"></div>
                        <div className="portfolio-caption">
                            <div className="title">
                                <h4>{data.title}</h4>
                            </div>
                            <div className="subtitle">
                                <span>{data.subtitle}</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div className="modal fade" id={`exampleModal${index}`} tabIndex="-1" role="dialog" aria-labelledby="" aria-hidden="true">
                <div className="modal-dialog modal-dialog-centered modal-lg" role="dialog">
                    <div className="modal-content col-md-12 p-0 m-0">
                        <div className="col-6 p-0 m-0">
                            <img src={data.img_source} className="img-fluid" alt="" />
                        </div>
                        <div className="col-6 m-0 dialog-content">
                            <h5>{data.title}</h5>
                            <p>{data.description}</p>
                            <a className="btn btn-secondary btn-block" href={data.link} target="_blank">Visit Website</a>
                        </div>
                    </div>
                </div>
            </div>
        </>
    )
}

export { Project };
