import React from "react"
import { Layout } from "../components/Layout"
import { Header } from "../components/Header"
import { Welcome } from "../components/Welcome"
import { About } from "../components/About"
import { Portfolio } from "../components/Portfolio"
import { Contact } from "../components/Contact"
import { Footer } from "../components/Footer"

export default function Home() {
  return (
    <Layout>
      <div className="fakeLoader"></div>
      <Header />
      <Welcome />
      <About />
      <Portfolio />
      <Contact />
      <Footer />
    </Layout >
  )
}
